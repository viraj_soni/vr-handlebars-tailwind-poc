const fs = require("fs");
const path = require("path");
const glob = require("glob");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

const config = require("./config");
const { normalizeText } = require("./utils/normalize");

const webpackConfig = {
  context: path.resolve(__dirname),
  entry: {
    main: "./src/js/global.js",
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "js/[name].js",
  },
  optimization: {
    minimizer: [new OptimizeCSSAssetsPlugin({})],
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: {},
          },
        ],
      },
      {
        test: /\.(handlebars|hbs)$/,
        loader: "handlebars-loader",
        query: {
          helperDirs: path.join(__dirname, "helpers"),
          partialDirs: [
            path.join(__dirname, "src"),
            path.join(__dirname, "src", "views"),
            path.join(__dirname, "src", "views", "ui"),
            path.join(__dirname, "src", "views", "layouts"),
            path.join(__dirname, "src", "views", "ui", "templates"),
            path.join(__dirname, "src", "views", "ui", "components"),
          ].concat(
            glob.sync("**/", {
              cwd: path.resolve(__dirname, "src", "views", "ui", "components"),
              realpath: true,
            })
          ),
        },
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
          },
        ],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        loader: "file-loader",
        options: {
          context: "./static",
        },
      },
      {
        test: /\.(ttf|eot|woff|woff2|svg)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            outputPath: "fonts/",
            esModule: false,
          },
        },
      },
      {
        test: /\.(scss|sass)$/,
        use: [
          "style-loader",
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: "./scss",
              hmr: process.env.NODE_ENV === "development",
            },
          },
          "css-loader",
          "resolve-url-loader",
          "sass-loader",
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss",
              plugins: [require("tailwindcss"), require("autoprefixer")],
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "css/[name].css",
      chunkFilename: "[id].css",
    }),
    new CopyPlugin([{ from: "./src/static" }]),
  ],
  devServer: {
    contentBase: path.join(__dirname, "dist"),
  },
};

glob
  .sync("**/*.hbs", {
    cwd: path.resolve(__dirname, "src", "views", "ui"),
    realpath: true,
  })
  .map((page) => {
    const fileName = path.basename(page);
    const pageName = fileName.split(".").slice(0, -1).join(".");

    // console.log(
    //   page,
    //   fs.readFileSync(path.join("mock-data", pageName, `${pageName}.json`))
    // );

    console.log(`Building page: ${fileName.toUpperCase()}`);

    const outputPath = path.relative("src/views", path.join(page, "../"));

    const dataFileExists = fs.existsSync(
      path.join("mock-data", pageName, `${pageName}.json`)
    );

    // if (dataFileExists) {
    //   const dataFile = fs.readFileSync(
    //     path.join("mock-data", pageName, `${pageName}.json`),
    //     { encoding: "utf8", flag: "r" }
    //   );
    //   console.log(dataFile);
    // }

    const htmlPageInit = new HtmlWebPackPlugin({
      title: `${normalizeText(pageName)}`,
      template: page,
      templateParameters:
        dataFileExists &&
        require(path.join(
          __dirname,
          "mock-data",
          pageName,
          `${pageName}.json`
        )),
      inject: process.env.NODE_ENV !== "production",
      filename: `./${pageName != "home" ? outputPath + "/" : ""}index.html`,
      chunks: ["main", pageName],
      // minify: config.htmlMinifyOptions,
    });

    // webpackConfig.entry[page] = `./src/views/components/${page}/${page}.js`;
    webpackConfig.plugins.push(htmlPageInit);
  });

// fs.readdirSync(path.join(__dirname, "src", "views", "components")).forEach(
//   (page) => {
//     console.log(page);y
//     // console.log(`Building page: ${page.toUpperCase()}`);

//     const htmlPageInit = new HtmlWebPackPlugin({
//       title: `${normalizeText(page)}`,
//       template: `./src/views/components/${page}/${page}.hbs`,
//       templateParameters: { pageTitle: "Hello" },
//       filename: `./ui/${page + "/"}index.html`,
//       // chunks: ["main", page],
//       // minify: config.htmlMinifyOptions,
//     });

//     // webpackConfig.entry[page] = `./src/views/components/${page}/${page}.js`;
//     webpackConfig.plugins.push(htmlPageInit);
//   }
// );

module.exports = webpackConfig;
