const fs = require("fs");
const path = require("path");
const glob = require("glob");
// const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  stories: ["../src/**/*.stories.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],
  addons: [
    "@storybook/addon-essentials",
    "@whitespace/storybook-addon-html/register",
    "@storybook/addon-actions/register",
    "@storybook/addon-knobs/register",
    "@storybook/addon-storysource/register",
    "@storybook/addon-viewport/register",
    "@storybook/addon-a11y/register",
  ],
  webpackFinal: async (config, { configType }) => {
    // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    // Make whatever fine-grained changes you need
    // config.module.rules.push({
    //   test: /\.scss$/,
    //   use: ['style-loader', 'css-loader', 'sass-loader'],
    //   include: path.resolve(__dirname, '../'),
    // });

    config.module.rules.push({
      test: /\.(handlebars|hbs)$/,
      loader: "handlebars-loader",
      query: {
        helperDirs: path.join(__dirname, "../helpers"),
        partialDirs: [
          path.join(__dirname, "../src"),
          path.join(__dirname, "../src", "views"),
          path.join(__dirname, "../src", "views", "ui"),
          path.join(__dirname, "../src", "views", "layouts"),
          path.join(__dirname, "../src", "views", "templates"),
          path.join(__dirname, "../src", "views", "components"),
        ].concat(
          glob.sync("**/", {
            cwd: path.resolve(__dirname, "../src", "views", "components"),
            realpath: true,
          })
        ),
      },
    });

    config.module.rules.push({
      test: /\.(scss|sass)$/,
      use: [
        "style-loader",
        // {
        //   loader: MiniCssExtractPlugin.loader,
        //   options: {
        //     publicPath: "./scss",
        //     hmr: process.env.NODE_ENV === "development",
        //   },
        // },
        "css-loader",
        "resolve-url-loader",
        "sass-loader",
        {
          loader: "postcss-loader",
          options: {
            ident: "postcss",
            plugins: [require("tailwindcss"), require("autoprefixer")],
          },
        },
      ],
    });

    // config.module.rules.push({
    //   test: /\.css$/,
    //   loaders: [
    //     {
    //       loader: "postcss-loader",
    //       options: {
    //         sourceMap: true,
    //         config: {
    //           path: "./.storybook/",
    //         },
    //       },
    //     },
    //   ],

    //   include: path.resolve(__dirname, "../storybook/"),
    // });

    // Return the altered config
    return config;
  },
};
