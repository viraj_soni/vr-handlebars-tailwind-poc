import "../src/scss/main.scss";

import { withHTML } from "@whitespace/storybook-addon-html/html";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
};

export const decorators = [withHTML];
