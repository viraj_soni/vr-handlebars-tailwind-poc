import Button from "../../views/ui/components/button/button.hbs";

export default {
  title: "Buttons",
};

export const PrimaryButton = ({ text }) =>
  Button({
    class: "btn-primary",
    text: "Button",
  });
PrimaryButton.args = {
  text: "Button",
};

export const SecondaryButton = ({ text }) =>
  Button({
    class: "btn-secondary",
    text: text,
  });
SecondaryButton.args = {
  text: "Button",
};
