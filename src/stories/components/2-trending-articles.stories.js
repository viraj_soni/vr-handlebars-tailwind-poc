import { action } from "@storybook/addon-actions";
import { linkTo } from "@storybook/addon-links";

import TrendingArticleTemplate from "../../views/ui/components/trending-article/trending-article.hbs";

export default {
  title: "Trending Article",
};

export const TrendingArticle = ({
  title,
  image,
  anchor,
  imageAltText,
  date,
  articleType,
  author,
}) =>
  `<div style="max-width:370px;">
    ${TrendingArticleTemplate({
      title,
      image,
      anchor,
      "image-alt-text": imageAltText,
      date,
      "article-type": articleType,
      author,
    })}
  </div>`;

TrendingArticle.args = {
  title:
    "Dividend Declared in Three Schemes of Aditya Birla Sun Life Mutual Fund",
  image: require("../../static/placeholder-cover.png"),
  anchor: "#",
  date: "26 Mar '19",
  articleType: "Interview",
  author: "Dhirendra Kumar",
};

export const TrendingArticleList = ({ articles }) => {
  return `
  <div class="flex flex-wrap container">
    <div class="w-2/3">
      <div class="flex flex-wrap">
        ${articles
          .map(
            (article) =>
              `
          <div class="w-1/2 pr-8 mb-5">
            ${TrendingArticleTemplate({
              title: article.title,
              image: require(`../../static/${article.image}`),
              anchor: article.anchor,
              "image-alt-text": article["image-alt-text"],
              date: article.date,
              "article-type": article["article-type"],
              author: article.author,
            })}
          </div>
          `
          )
          .join("")}
      </div>
    </div>
    <div class="w-1/3"></div>
  </div>
`;
};

TrendingArticleList.args = {
  articles: require("../../../mock-data/home/home.json").articles,
};
