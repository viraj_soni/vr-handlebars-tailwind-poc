module.exports = {
  purge: [],
  theme: {
    extend: {},
    fontFamily: {
      cooper: ["Cooper Lt BT"],
      ibmPlexSans: ["IBM Plex Sans", "sans-serif"],
    },
    fontSize: {
      fs1: "50px",
      fs2: "40px",
      fs3: "32px",
      fs4: "24px",
      fs5: "20px",
      fs6: "14px",
      fs7: "12px",
    },
    colors: {
      transparent: "transparent",
      blue: {
        1: "#1C509D",
        2: "#153C76",
        3: "#8DA7CE",
        4: "#2266CA",
        5: "rgba(28, 77, 160, 0.15)",
        6: "#B8E2F8",
        7: "rgba(28, 77, 160, 0.30)",
      },
      black: {
        100: "#000000",
        60: "rgba(0, 0, 0, 0.6)",
        25: "rgba(0, 0, 0, 0.25)",
        10: "rgba(0, 0, 0, 0.10)",
      },
      white: {
        100: "#ffffff",
        60: "rgba(255, 255, 255, 0.75)",
        25: "rgba(255, 255, 255, 0.30)",
      },
      peach: "#FBC6A6",
      lightPeach: "#FEF1E8",
      lightGray: "#E1E2E3",
      purple: {
        1: "#811CA0",
        2: "rgba(129, 28, 160, 0.15)",
      },
      pink: {
        1: "#F0547F",
        2: "rgba(240, 84, 127, 0.15)",
      },
      red: "#B91014",
      mediumYellow: "#C88E34",
    },
  },
  variants: {},
  plugins: [],
};
